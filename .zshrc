#################################
Oh-my-zsh config file 

1) Install oh-my-zsh (https://github.com/robbyrussell/oh-my-zsh)
2) Install cobalt2 theme (https://github.com/wesbos/Cobalt2-iterm)
3) Download z.sh and put in home directory (https://github.com/rupa/z)
4) Set ZSH_THEME="cobalt2"
5) Add the following Alias's

#################################

# Custom Alias
alias subl="/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl"
alias ll="ls -l -a"

alias gitclean="git branch --merged | grep -v \"\*\" | grep -v master | grep -v dev | xargs -n 1 git branch -d && git remote prune origin"
alias gitrbm="git rebase -i master"

alias glmaster="git pull https://gitlab.com/gitlab-org/gitlab-ce.git"

# include Z
. ~/z.sh